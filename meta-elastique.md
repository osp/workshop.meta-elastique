% Bienvennu·e·s!
% Les lignes commençant par le signe `%` ne sont pas prises en compte par metapost.
% Elles peuvent donc servir à mettre des commentaires

% Quelques liens lettrages et signalétique


% Une documentation Metapost
% Des ressources et exemples Metapost% locale cheatsheet →% en ligne http://tex.loria.fr/prod-graph/zoonekynd/metapost/metapost.html et https://melusine.eu.org/syracuse/metapost/mpman/
% en ligne http://tex.loria.fr/prod-graph/zoonekynd/metapost/metapost.html et https://melusine.eu.org/syracuse/metapost/mpman/

outputtemplate := "svg/%c.svg"; outputformat := "svg";

u = 21pt;
ux = 1*u;
uy = 1*u;
strokeX = 3*u;
strokeY = 1.5*u;
rotation = 0;

height = 40;
grid = 1;


color pinkrose; pinkrose:= (255/102,153/255,200/255) ;

def beginchar(expr keycode, width)=
  beginfig(keycode);
        pickup pencircle scaled 1;

        draw (0 * ux, 0 * uy) -- (width * ux, 0 * uy) -- (width * ux, height * uy) -- (0 * ux, height * uy) -- cycle scaled 0 withcolor red;
        height = 40;
        if grid = 1:
                defaultscale :=.2;
                for i=0 upto width:
                        draw (i*ux, height*uy) -- (i*ux, 0*uy) withcolor pinkrose;
                endfor;
                for i=0 upto height:
                        draw (width*ux, i*uy) -- (0*ux, i*uy) withcolor pinkrose;
                endfor;
        fi;

        pickup pencircle xscaled sx yscaled sy rotated rot;

enddef;

def endchar(expr lenDots)=
        if grid = 1:
            defaultscale :=.1*u;
                for i=1 upto lenDots:
                        dotlabels.urt([i]) withcolor blue;
                endfor;
        fi;
            endfig;

enddef;


% #
beginchar(35, 22);
    x1 := 11.0 * ux;
    y1 := 43.0 * uy;
    x2 := 4.0 * ux;
    y2 := 11.0 * uy;
    x3 := 17.0 * ux;
    y3 := 43.0 * uy;
    x4 := 10.0 * ux;
    y4 := 11.0 * uy;
    x5 := 4.0 * ux;
    y5 := 30.0 * uy;
    x6 := 18.0 * ux;
    y6 := 30.0 * uy;
    x7 := 3.0 * ux;
    y7 := 24.0 * uy;
    x8 := 17.0 * ux;
    y8 := 24.0 * uy;
                draw z1 -- z2;
                draw z3 -- z4;
                draw z5 -- z6;
                draw z7 -- z8;

endchar(8);

end;
